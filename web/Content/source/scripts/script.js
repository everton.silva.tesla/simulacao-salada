$(function () {

    'use strict';

    $('html').removeClass('no-js');

	/**
	/* Seleciona todos elementos que possuem atributo "data-src"
	/* e gera uma imagem dentro do elemento selecionado.
	/* A imagem será carregada após o body estar completo.
	/* Utilize se a imagem for pesada e/ou necessite atrasar o carregamento da mesma.
	**/
    $('[data-img]').each(dataImg);

	/**
	/* Versão alternativa para navegadores antigos que não suportam "placeholder".
	**/
    $('.lt-ie10 [placeholder]').focus(placeholderFocus).blur(placeholderBlur).trigger('blur');
    $('.lt-ie10 [placeholder]').parents('form').submit(placeholderSubmit);

    inputMasks();


    $('#form-contato').validate({
        rules: {
            BirthDate: { dateBR: true }
        },
        focusInvalid: false,
        invalidHandler: function (event, validator) {
            var name;
            $(validator.errorList).each(function (i) {
                name = $(this)[0].element.name.replace('[', '\\[').replace(']', '\\]');
                if ($('[name="' + name + '"]').parent().hasClass('select-custom'))
                    $('[name="' + name + '"]').parent().addClass('error');

                if ($('[name="' + name + '"]').parent().hasClass('file-custom'))
                    $('[name="' + name + '"]').parent().addClass('error');
            });

            name = validator.errorList[0].element.name.replace('[', '\\[').replace(']', '\\]');

            var offset = $(this).find('[name="' + name + '"]').offset();
            $('html, body').animate({
                scrollTop: (offset.top - 100)
            }, 1000);

            $('div.select-custom.error .select, .file-custom.error .file').change(function () {
                if ($(this).val().length)
                    $(this).parent().removeClass('error');
            });
        },
        errorPlacement: function (error, element) {
            //element.closest('.form-group').append(error);
            return true;
        },
        submitHandler: function (form) {
            if (grecaptcha.getResponse() == '') {
                $('.g-recaptcha').addClass('error').append('<span class="error">O captcha é obrigatório.</span>');
            } else {
                $('.g-recaptcha').removeClass('error').find('span.error').remove();
                $(form).find('button[type="submit"]').attr('disabled', 'disabled');
                form.submit();
            }
        }
    });

    $('#form-search form').validate({
        rules: {
            busca: 'required'
        },
        messages: {
            busca: ''
        }
    });


    if (getCookie('outdated') != 'warned') {
        outdatedBrowser({
            bgColor: '#f25648',
            color: '#ffffff',
            lowerThan: 'transform',
            languagePath: 'pt-br.html'
        });
        setCookie('outdated', 'warned', 365);
    }

    var url = window.location.href;
    if (getParameterByName('momento', url) != null || getParameterByName('busca', url) != null) {
        $(window).load(function () {
            $('html, body').animate({ scrollTop: $('.infinite-scroll').offset().top }, 2000);
        });
    }


    // MENU 
    // Abrir menu
    $('#open-menu').on('click', function () {
        if ($(this).hasClass('fechar')) {
            $('body').removeClass('mobmenu offscroll');
            $('.menu').removeClass('open');
            $('#open-menu').removeClass('fechar');
        } else {
            $('body').addClass('mobmenu offscroll');
            $('.menu').addClass('open');
            $(this).addClass('fechar');
        }
    });

    // Abrir busca
    $('#open-busca').on('click', function () {
        if ($(this).hasClass('fechar')) {
            $('body').removeClass('mobbusca offscroll');
            $('#form-search').removeClass('open');
            $('#open-busca').removeClass('fechar');
            console.log('t');
        } else {
            $('body').addClass('mobbusca offscroll');
            $('#form-search').addClass('open');

            $(this).addClass('fechar');
            console.log('tt');
        }
    });






    // HOME
    $('.banner').slick({
        dots: true,
        infinite: false,
        nextArrow: '<a class="slick-next"><i class="icon icon-seta-next"></i></a>',
        prevArrow: '<a class="slick-prev"><i class="icon icon-seta-prev"></i></a>'
    });

    $('.carousel-produtos-maionese').slick({
        dots: true,
        asNavFor: '.carousel-receitas-maionese',
        nextArrow: '<a class="slick-next"><i class="icon icon-seta-next"></i></a>',
        prevArrow: '<a class="slick-prev"><i class="icon icon-seta-prev"></i></a>'
    });

    $('.carousel-receitas-maionese').slick({
        arrows: false,
        asNavFor: '.carousel-produtos-maionese'
    });

    $('.carousel-produtos-oleo').slick({
        dots: true,
        asNavFor: '.carousel-receitas-oleo',
        nextArrow: '<a class="slick-next"><i class="icon icon-seta-next"></i></a>',
        prevArrow: '<a class="slick-prev"><i class="icon icon-seta-prev"></i></a>'
    });

    $('.carousel-receitas-oleo').slick({
        arrows: false,
        asNavFor: '.carousel-produtos-oleo'
    });

    $('.carousel-momentos-receitas').slick({
        arrows: false,
        asNavFor: '.carousel-momentos'
    });

    $('.carousel-momentos').slick({
        dots: true,
        asNavFor: '.carousel-momentos-receitas',
        nextArrow: '<a class="slick-next"><i class="icon icon-seta-next"></i></a>',
        prevArrow: '<a class="slick-prev"><i class="icon icon-seta-prev"></i></a>'
    });


    carregaImgBanner(); // Carrega img do banner ao inicializar 
    $(window).resize(function () {
        carregaImgBanner(); // Carrega img do banner ao redimensionar 
    });



    // CONTATO
    $('#estado').change(function () {
        console.log($(this).val());
        $.ajax({
            type: "GET",
            url: 'Contato/GetCitiesByState?stateId=' + $(this).val(),
            dataType: 'json',
            success: function (response) {
                var html = '';
                for (var i = 0; response.length > i; i++) {
                    html += '<option value="' + response[i].ID + '">' + response[i].Name + '</option>';
                }
                $("#cidade").html(html);
            }
        });
    });




    // SOBRE
    $('.linha-tempo-carosel').slick({
        dots: true,
        nextArrow: '<a class="slick-next"><i class="icon icon-seta-next"></i></a>',
        prevArrow: '<a class="slick-prev"><i class="icon icon-seta-prev"></i></a>'
    });




    // RECEITAS
    $('.infinite-scroll').jscroll({
        loadingHtml: '<div class="col-sm-12 col-md-12 text-center"><a class="btn btn-laranja btn-carregando">Carregando...</a></div>',
        autoTrigger: false,
        nextSelector: 'a.jscroll-next:last'
    });



    $(window).scroll(function () {
        dataBg();
    });

    if ($('.produto.maionese-salada').length || $('.produto.maionese-pro').length) {
        var product = $('body').attr('class').replace(/ /g, "-");

        $('.embalagens li:first-of-type').addClass('active');
        $('.embalagens li').on('click', function () {
            $(this).parents('.interna').find('.embalagens li').removeClass('active');
            $(this).addClass('active');
            $(this).parents('.interna').find('.image img').attr('src', '/content/assets/images/produtos/embalagem-' + product + '-' + $(this).index() + '.png');
        });
    }
});

// Troca imagem do banner
function carregaImgBanner() {

    var $this = $('#home-banner .slick-slide'),
        wWindow = $(window).width();

    if (wWindow < 768) {
        $this.each(function (index) {
            $(this).css('background-image', 'url(' + $(this).attr('data-img-mobile') + ')');
        });
    } else {
        $this.each(function (index) {
            $(this).css('background-image', 'url(' + $(this).attr('data-img-desktop') + ')');
        });
    }

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function fitRows($container, options) {

    var cols = options.numColumns,
        $els = $container.children(),
        maxH = 0, j;

    $els.each(function (i, p) {

        var $p = $(p), h;

        $p.css('min-height', '');

        maxH = Math.max($p.outerHeight(true), maxH);
        if (i % cols == cols - 1 || i == cols - 1) {
            for (j = cols; j; j--) {
                $p.css('min-height', maxH);
                $p = $p.prev();
            }
            maxH = 0;
        }

    });
}

function inputMasks() {

    $('input.tel')
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });

    $('input.cep').mask('99999-999');

    $('input.data').mask('99/99/9999');
}

function dataImg() {
    var $this = $(this),
        obj = eval(['(', $this.attr('data-img'), ')'].join('')),
        keys = Object.keys(obj), img_src, current,
        width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    while (!img_src && keys.length) {
        current = keys.pop();
        if (current <= width) img_src = obj[current];
    }

    if (!$this.has(['.data-img-', current].join('')).length) {
        $this
            .find('.data-img').remove().end()
            .append(
            $(['<img class="data-img data-img-', current, '" src="', img_src, '" />'].join(''))
                .one('load', loadImg)
            );
    }

    function loadImg() {
        $this.addClass('complete');
    }
}

var carregouDataBg = false;
function dataBg() {
    if (carregouDataBg == false) {
        $('*[data-background]').each(function () {
            $(this).css('background-image', 'url(' + $(this).data('background') + ')');
        });
    }
    carregouDataBg = true;
}

function placeholderFocus() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');

        if (input.hasClass('password'))
            input.attr('type', 'password').removeClass('password');
    }
}

function placeholderBlur() {
    var input = $(this);
    if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));

        if (input.attr('type') == 'password')
            input.attr('type', 'text').addClass('password');
    }
}

function placeholderSubmit() {
    $(this).find('[placeholder]').each(placeholderEach);
}

function placeholderEach() {
    var input = $(this);
    if (input.val() == input.attr('placeholder'))
        input.val('');
}

function setCookie(name, value, days) {
    var date = new Date();
    date.setDate(date.getDate() + days);

    var cookie = escape(value) + ((days == null) ? '' : '; expires=' + date.toUTCString());
    document.cookie = name + '=' + cookie;
}

function getCookie(name) {
    var cookie = document.cookie;
    var start = cookie.indexOf(" " + name + "=");

    if (start == -1) {
        start = cookie.indexOf(name + "=");
    }

    if (start == -1) {
        cookie = null;
    } else {
        start = cookie.indexOf("=", start) + 1;

        var end = cookie.indexOf(";", start);

        if (end == -1) {
            end = cookie.length;
        }

        cookie = unescape(cookie.substring(start, end));
    }

    return cookie;
}

if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [];

        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                keys.push(i);
            }
        }

        return keys;
    };
}

